Rails.application.routes.draw do
  root 'main#index'
  get '/auth/:provider/callback', to: 'session#create'
  get '/task/list', to: 'task#list'
  get '/task/:id/resolve', to: 'task#resolve'
end
