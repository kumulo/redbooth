class TaskController < ApplicationController
  before_action :require_login

  def list
    @tasks = current_user.tasks
  end

  def resolve
    Task.new(current_token).resolve!(params[:id])
    redirect_to '/task/list'
  end

  protected

  def require_login
    redirect_to '/auth/redbooth' if current_token.nil?
  end
end