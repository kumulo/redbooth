class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_token
    @_current_token ||= session[:current_token]
  end

  def current_user
    return if current_token.nil?
    @_current_user ||= User.new(current_token)
  end
end
