class SessionController < ApplicationController
  def create
    session[:current_token]             = auth_token
    session[:current_token_expiration]  = auth_token_expiration
    if session[:current_token].nil?
      redirect_to '/'
    else
      redirect_to '/task/list'
    end
  end

  protected

  def auth_token
    request.env['omniauth.auth']['credentials']['token']
  rescue
    nil
  end

  def auth_token_expiration
    request.env['omniauth.auth']['credentials']['expires_at']
  rescue
    nil
  end
end