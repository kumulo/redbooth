class Task < RedboothModel

  def resolve!(id)
    client.task(:update, id: id, status: 'resolved')
  end

end