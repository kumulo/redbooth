class RedboothModel
  def initialize(token)
    @_token = token
  end

  protected

  def client
    @_client ||= RedboothRuby::Client.new(session)
  end

  def session
    @_session ||= RedboothRuby::Session.new(token: @_token)
  end

end