require 'rails_helper'

RSpec.describe TaskController, :type => :controller do
  let!(:tasks) { [ 'a', 'b', 'c' ] }
  let!(:user)  { double('user', tasks: tasks) }

  before do
    allow(controller).to receive(:current_token) { 'tooooken' }
    allow(controller).to receive(:current_user) { user }
  end

  describe 'GET #list' do
    it 'responds successfully with an HTTP 200 status code' do
      get :list

      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it 'renders the list template' do
      get :list

      expect(response).to render_template('list')
    end

    it 'loads all of the tasks into @tasks' do
      get :list

      expect(assigns(:tasks)).to match_array(tasks)
    end
  end

end
