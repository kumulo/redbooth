require 'rails_helper'

RSpec.describe SessionController, :type => :controller do
  describe 'GET #create' do
    let(:omniauth) { OmniAuth.config.mock_auth[:redbooth] }

    before do
      request.env['omniauth.auth'] = omniauth
    end

    describe 'Authentication correct' do
      it 'redirects to task list' do
        get :create, provider: 'redbooth'

        expect(response).to redirect_to('/task/list')
      end
    end

    describe 'Invalid authentication' do
      let(:omniauth) { [] }

      it 'save the token on session' do
        get :create, provider: 'redbooth'

        expect(response).to redirect_to('/')
      end
    end
  end

end
