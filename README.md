##Rails Redbooth api integration test
-------------------------------------

###Getting Oauth credentials
First thing you need to do it's getting your own oauth app credentials. To do so please follow up the intructions given in the redbooth platform site

https://redbooth.com/api/getting-started/

Add these credentials to environment variables:
```sh
export REDBOOTH_APP_ID="my_app_id"
export REDBOOTH_APP_SECRET="my_app_secret_id"
```
* Use setenv syntax for linux

###Create some example tasks
Go to your redbooth account and create some sample tasks

###Run web version
Just start rails with
```sh
rails s
```
###What's next
- [ ] Build a rake task to populate Redbooth with sample data
- [ ] Add a log out button
- [ ] In order to not collide with your existing tasks, create a new project and work only over tasks on it.
- [ ] Manage unauthorized exceptions
- [ ] Manage expiring tokens (https://github.com/teambox/omniauth-redbooth/blob/1b6c05610d2f80681f6b709eda392143550a7ea4/examples/config.ru#L29)
- [ ] Manage not valid tokens
- [ ] Add extra tests for models
